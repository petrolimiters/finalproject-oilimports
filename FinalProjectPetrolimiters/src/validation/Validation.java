/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validation;

import javax.swing.JOptionPane;

/**
 *
 * @author prathmesh
 */
public class Validation {
    public static boolean isValidAmountString(String input) {
        if (input.length() == 0) {
            
            JOptionPane.showMessageDialog(null, "Cannot be a null value");
            return false;
        }
        try {
            if (Double.parseDouble(input) < 0) {
                    JOptionPane.showMessageDialog(null, "Cannot be a null value");
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    
    
    
    public static boolean validateInt(String value){
        
       try{
            int val = Integer.parseInt(value);
            return true;
        }catch(NumberFormatException e){
            JOptionPane.showMessageDialog(null, "Enter Valid Integer Value");
            return false;
        }
    }
    
    public static boolean validateDouble(String value){
        
       try{
            double val = Double.parseDouble(value);
            return true;
        }catch(NumberFormatException e){
            JOptionPane.showMessageDialog(null, "Enter Valid Integer Value");
            return false;
        }
    }
 
    
    
    
    
}
