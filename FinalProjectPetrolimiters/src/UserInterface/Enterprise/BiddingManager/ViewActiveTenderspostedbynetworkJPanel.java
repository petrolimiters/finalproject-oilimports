/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Enterprise.BiddingManager;

import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.BiddingRequest;
import Business.WorkQueue.TenderRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CHANDNI
 */
public class ViewActiveTenderspostedbynetworkJPanel extends javax.swing.JPanel {

    private JPanel subUserProcessContainer;
    private Organization org;
    private UserAccount ua;
    private Enterprise enterprise;
    private OrganizationDirectory netOrgDir;

    /**
     * Creates new form ViewActiveTenders
     */
    public ViewActiveTenderspostedbynetworkJPanel(JPanel subUserProcessContainer, UserAccount ua, Organization org, OrganizationDirectory netOrgDir, Enterprise enterprise) {
        initComponents();
        this.subUserProcessContainer = subUserProcessContainer;
        this.org = org;
        this.ua = ua;
        this.enterprise = enterprise;
        this.netOrgDir = netOrgDir;
        populateTbl();
        populateTblcheckbids();
    }

    public void populateTbl() {
        DefaultTableModel dtm = (DefaultTableModel) TenderTable.getModel();
        dtm.setRowCount(0);
        Date time = dateChooserCombo1.getSelectedDate().getTime();
        Date time1 = dateChooserCombo2.getSelectedDate().getTime();
        for (WorkRequest request : org.getWorkQueue().getWorkRequestList()) {
            if (request instanceof TenderRequest) {
                Object[] row = new Object[5];
                TenderRequest tRequest = (TenderRequest) request;
                if (tRequest.getStatus().equals(TenderRequest.status.Open.getValue())
                        && tRequest.getTenderType().getValue().equals(TenderRequest.TenderType.Local.getValue())) {
                    if ((tRequest.getRequestDate().after(time) || tRequest.getRequestDate().equals(time)) 
                            && (tRequest.getRequestDate().before(time1)) || tRequest.getRequestDate().equals(time1)) {
                        row[0] = tRequest;
                        row[1] = tRequest.getOilQuantity();
                        row[2] = tRequest.getQuotePrice();
                        row[3] = tRequest.getRequestDate();
                        row[4] = tRequest.getResolveDate();
                        dtm.addRow(row);
                    }
                }
            }
        }
    }

    public void populateTblcheckbids() {
        DefaultTableModel dtm = (DefaultTableModel) checkacceptedbidstbl.getModel();
        dtm.setRowCount(0);
        Date time = dateChooserCombo1.getSelectedDate().getTime();
        Date time1 = dateChooserCombo2.getSelectedDate().getTime();
        for (WorkRequest request : org.getWorkQueue().getWorkRequestList()) {
            if (request instanceof BiddingRequest) {
                Object[] row = new Object[5];
                BiddingRequest tRequest = (BiddingRequest) request;
                if (tRequest.getStatus().equals(BiddingRequest.status.Accepted.getValue())) {
                    if ((tRequest.getRequestDate().after(time) || tRequest.getRequestDate().equals(time)) 
                            && (tRequest.getRequestDate().before(time1)) || tRequest.getRequestDate().equals(time1)) {
                       row[0] = tRequest.getTenderName();
                        row[1] = tRequest;
                        row[2] = tRequest.getBidPrice();
                        row[3] = tRequest.getStatus();
                        dtm.addRow(row);
                    }
                }
            }
        }
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        TenderTable = new javax.swing.JTable();
        placeBidJButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        dateChooserCombo1 = new datechooser.beans.DateChooserCombo();
        dateChooserCombo2 = new datechooser.beans.DateChooserCombo();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        checkacceptedbidstbl = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setBackground(new java.awt.Color(59, 89, 152));

        TenderTable.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        TenderTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Tender Name", "Tender oil Quantity", "Tender min Price", "Tender Start Date", "Tender End Date", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(TenderTable);

        placeBidJButton.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        placeBidJButton.setText("Place Bids");
        placeBidJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                placeBidJButtonActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Start Date:");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 3, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("View Active Tenders");
        jLabel3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel3.setVerticalTextPosition(javax.swing.SwingConstants.TOP);

        checkacceptedbidstbl.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        checkacceptedbidstbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Tender Name", "BidID", "BidPrice", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(checkacceptedbidstbl);

        jLabel2.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("End Date:");

        jButton1.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        jButton1.setText("Refresh");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(350, 350, 350))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(286, 286, 286)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(placeBidJButton)
                                .addGap(74, 74, 74))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(dateChooserCombo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addGap(18, 18, 18)
                                    .addComponent(dateChooserCombo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 685, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 685, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 26, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(104, 104, 104)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(dateChooserCombo1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(dateChooserCombo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(51, 51, 51)
                        .addComponent(placeBidJButton)))
                .addContainerGap(189, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void placeBidJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_placeBidJButtonActionPerformed
        // TODO add your handling code here:
        int selectedRow = TenderTable.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Select a Tender from Table...!");
            return;
        }
        TenderRequest tender = (TenderRequest) TenderTable.getValueAt(selectedRow, 0);
        PlacingBidAsEnterpriseJPanel pbae = new PlacingBidAsEnterpriseJPanel(subUserProcessContainer, tender, org, ua, netOrgDir, enterprise);
        subUserProcessContainer.add("PlacingBidAsEnterpriseJPanel", pbae);
        CardLayout layout = (CardLayout) subUserProcessContainer.getLayout();
        layout.next(subUserProcessContainer);
    }//GEN-LAST:event_placeBidJButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
populateTbl();
populateTblcheckbids();// TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TenderTable;
    private javax.swing.JTable checkacceptedbidstbl;
    private datechooser.beans.DateChooserCombo dateChooserCombo1;
    private datechooser.beans.DateChooserCombo dateChooserCombo2;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton placeBidJButton;
    // End of variables declaration//GEN-END:variables
}
