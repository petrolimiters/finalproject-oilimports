/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Network.Network;
import Business.Network.networkanalysisDirectory;
import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author kaushikp
 */
public abstract class Role {
    
    private RoleType roleType;
    
    public enum RoleType{
        SystemAdmin("SystemAdmin"),
        BiddingManager("BiddingManager"),
        Analyst("Analyst"),
        TransportManager("TransportManager"),
        TenderRP("TenderRP");
        
        private String value;
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public RoleType getRoleType() {
        return roleType;
    }
    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            UserAccount account,
            Network network,
            Organization organization,
            EnterpriseDirectory enterpriseDirectory,
            Enterprise enterprise, 
            EcoSystem business);
    
    

    @Override
    public String toString() {
        return getClass().getName();
    }
    
    
}
