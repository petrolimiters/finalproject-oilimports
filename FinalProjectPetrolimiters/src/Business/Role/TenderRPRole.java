/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.Network.TenderPoster.NetworkTenderRPWorkAreaJPanel;
import UserInterface.WOI.TenderPoster.WOITenderRPworkarea;
import javax.swing.JPanel;

/**
 *
 * @author kaushikp
 */
public class TenderRPRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Network network, Organization organization, EnterpriseDirectory enterpriseDirectory, Enterprise enterprise, EcoSystem business) {
   if(enterprise==null && network == null)
            return new WOITenderRPworkarea(userProcessContainer, account, business.getOrganizationDirectory(), business.getNetworkDir());
        else if(enterprise == null && network!=null)
            return new NetworkTenderRPWorkAreaJPanel(userProcessContainer, account, network);
        
   else
            return null;
    }

    
    
}
