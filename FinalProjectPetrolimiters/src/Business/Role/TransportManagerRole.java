/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.Enterprise.TransportManager.ViewAcceptedBidsEnterprise;
import UserInterface.Network.TransportManager.NetworkTransportManagerWorkAreaJPanel;
import UserInterface.WOI.Transport.ViewWOITransportationRequestJPanel;
import javax.swing.JPanel;

/**
 *
 * @author kaushikp
 */
public class TransportManagerRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Network network, Organization organization, EnterpriseDirectory enterpriseDirectory, Enterprise enterprise, EcoSystem business) {
if(enterprise == null && network == null)
            return new ViewWOITransportationRequestJPanel(userProcessContainer, organization);
        //return new test(userProcessContainer, business.getOrganizationDirectory(), business.getMasterUserAccountDirectory());
        else if(enterprise == null && network!=null)
            return new NetworkTransportManagerWorkAreaJPanel(userProcessContainer, network.getOrganizationDirectory(), account, business.getOrganizationDirectory(), organization);
        else
            return new ViewAcceptedBidsEnterprise(userProcessContainer, enterprise.getOrgDir(), account, network.getOrganizationDirectory(), organization);    }

    
}
