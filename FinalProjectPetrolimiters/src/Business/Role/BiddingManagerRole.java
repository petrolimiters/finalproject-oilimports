/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.Enterprise.BiddingManager.ViewActiveTenderspostedbynetworkJPanel;
import UserInterface.Network.BiddingManager.NetworkBidManagerWorkareaJPanel;
import UserInterface.WOI.BidManager.WOIBidManagerWorkareaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author kaushikp
 */
public class BiddingManagerRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Network network, Organization organization, EnterpriseDirectory enterpriseDirectory, Enterprise enterprise, EcoSystem business) {
        if(enterprise == null && network == null)
            return new WOIBidManagerWorkareaJPanel(userProcessContainer, organization);
        else if(enterprise == null && network!=null)
            return new NetworkBidManagerWorkareaJPanel(userProcessContainer, organization, account, business.getOrganizationDirectory(), network);
        else
            //Need to create Bid Manager WorkArea at Enterprise Level
            return new ViewActiveTenderspostedbynetworkJPanel(userProcessContainer, account, organization, network.getOrganizationDirectory(), enterprise); 
    }

    
}
