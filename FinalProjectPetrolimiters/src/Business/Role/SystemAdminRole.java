/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Network.Network;
import Business.Network.networkanalysisDirectory;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.Enterprise.Admin.EnterpriseAdminWorkAreaJPanel;
import UserInterface.Network.Admin.NetworkAdminWorkAreaJPanel;
import UserInterface.WOI.Admin.AdminWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author kaushikp
 */
public class SystemAdminRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Network network, Organization organization, EnterpriseDirectory enterpriseDirectory, Enterprise enterprise, EcoSystem business) {
        if(enterprise == null && network == null)
            return new AdminWorkAreaJPanel(userProcessContainer, account, business.getOrganizationDirectory(), business.getNetworkDir(), business.getMasterUserAccountDirectory());
        //return new test(userProcessContainer, business.getOrganizationDirectory(), business.getMasterUserAccountDirectory());
        else if(enterprise == null && network!=null)
            return new NetworkAdminWorkAreaJPanel(userProcessContainer, network, business.getMasterUserAccountDirectory());
        else
            return new EnterpriseAdminWorkAreaJPanel(userProcessContainer, enterprise, business.getMasterUserAccountDirectory());
    }

    
}
