/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Network.Network;
import Business.Network.networkanalysisDirectory;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.Enterprise.Analyst.EnterprisepreviousandcurrentanalysisJPanel;
import UserInterface.Network.Analyst.NetworkAnalysisWorkAreaNewJPanel;
import javax.swing.JPanel;

/**
 *
 * @author kaushikp
 */
public class AnalystRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Network network, Organization organization, EnterpriseDirectory enterpriseDirectory, Enterprise enterprise, EcoSystem business) {
//if(enterprise == null && network == null)
        //return new test(userProcessContainer, business.getOrganizationDirectory(), business.getMasterUserAccountDirectory());
         if(enterprise == null && network!=null)
            return new NetworkAnalysisWorkAreaNewJPanel(userProcessContainer, account, organization, network, business.getNetworkDir(), network.getNadir());
        else
            return new EnterprisepreviousandcurrentanalysisJPanel(userProcessContainer,network, business.getNetworkDir(), enterprise.getEnteranadir(), enterprise);
    }

    
    
}
