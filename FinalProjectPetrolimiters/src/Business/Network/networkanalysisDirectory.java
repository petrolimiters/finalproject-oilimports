/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import loadCSV.CSV;

/**
 *
 * @author prathmesh
 */
public class networkanalysisDirectory {
        private ArrayList<NetworklevelAnalysis> oilinventory;
        private ArrayList<NetworklevelAnalysis> oilinventoryincoming;
        

        
        public networkanalysisDirectory(String country) 
                
        {
                    oilinventory = new ArrayList<>();
                    oilinventoryincoming = new ArrayList<>();
                  
            
                        try {
                            if(country.equalsIgnoreCase("India"))
                  {
                            CSV.loaddataindia(this);
                  }
                            else
                            
                               
                            CSV.loaddata(this); 
                        } catch (IOException ex) {
                            Logger.getLogger(networkanalysisDirectory.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                    
            
                  }

        
        
        public NetworklevelAnalysis AddNetworkanalysis(double im,double ex,double fpp,double ogp,double icop, String year){
        NetworklevelAnalysis n = new NetworklevelAnalysis();
        n.setNetimports((int) im);
        n.setFirstpurchaseprice(fpp);
        n.setNetexports((int) ex);
        n.setOilglobalprice(ogp);
        n.setImportedcrudeoilprice(icop);
        n.setYear(year);
        oilinventory.add(n);
        return n;
    }
        

        
        public NetworklevelAnalysis addincomingoilinventoryvalues(double im,double ex,double icop, String year){
         NetworklevelAnalysis n = new NetworklevelAnalysis();
        n.setNetimports((int) im);
        n.setNetexports((int)ex);
        n.setImportedcrudeoilprice(icop);
        n.setYear(year);
            NetworklevelAnalysis matchYear = matchYear(year);
        n.setFirstpurchaseprice(matchYear.getFirstpurchaseprice());
        n.setOilglobalprice(matchYear.getOilglobalprice());
        oilinventoryincoming.add(n);
        return n;
    }
        
        
        public NetworklevelAnalysis matchYear(String year)
        {
            for(NetworklevelAnalysis mn : oilinventory)
            {
                if (mn.getYear().equals(year))
                {
                    return mn;
                }
            }
            return null;
        }
        

    public ArrayList<NetworklevelAnalysis> getOilinventoryincoming() {
        return oilinventoryincoming;
    }

    public void setOilinventoryincoming(ArrayList<NetworklevelAnalysis> oilinventoryincoming) {
        this.oilinventoryincoming = oilinventoryincoming;
    }
        
        

    public ArrayList<NetworklevelAnalysis> getOilinventory() {
        return oilinventory;
    }

    public void setOilinventory(ArrayList<NetworklevelAnalysis> oilinventory) {
        this.oilinventory = oilinventory;
    }
        
        

    
}
