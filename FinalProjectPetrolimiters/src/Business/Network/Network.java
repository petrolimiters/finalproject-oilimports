/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import Business.Enterprise.EnterpriseDirectory;
import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import Business.Role.Role;
import java.util.ArrayList;
import loadCSV.CSV;

/**
 *
 * @author kaushikp
 */
public class Network extends Organization {
    
    private String netWorkId;
    private String country;
    public static int importlimit=7000;
    private NetworklevelAnalysis na;
    private networkanalysisDirectory nadir;
    private EnterpriseDirectory entDir;
    private OrganizationDirectory organizationDirectory;
    private static int nwcount=0;
   

    
    public Network(String name, String country) {
        super(name);
                 this.country=country;
        entDir = new EnterpriseDirectory(country);
        organizationDirectory = new OrganizationDirectory();
        netWorkId = "NID00" + ++nwcount;
        // na = new NetworklevelAnalysis(double im);
         nadir = new networkanalysisDirectory(country);
        
    }

    public NetworklevelAnalysis getNa() {
        return na;
    }

    public void setNa(NetworklevelAnalysis na) {
        this.na = na;
    }

    public networkanalysisDirectory getNadir() {
        return nadir;
    }

    public void setNadir(networkanalysisDirectory nadir) {
        this.nadir = nadir;
    }
 

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }

    public void setOrganizationDirectory(OrganizationDirectory organizationDirectory) {
        this.organizationDirectory = organizationDirectory;
    }

    public String getNetWorkId() {
        return netWorkId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    public EnterpriseDirectory getEntDir() {
        return entDir;
    }

    public void setEntDir(EnterpriseDirectory entDir) {
        this.entDir = entDir;
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String toString(){
        return getName();
    }
    
}
