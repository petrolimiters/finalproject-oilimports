/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import java.util.ArrayList;

/**
 *
 * @author kaushikp
 */
public class NetworkDirectory {
    
    private ArrayList<Network> networkList;
    
    public NetworkDirectory(){
        networkList = new ArrayList<>();
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public void setNetworkList(ArrayList<Network> networkList) {
        this.networkList = networkList;
    }
    
    public Network createAndAddNetwork(String name, String check){
        Network n = new Network(name, check);
        networkList.add(n);
        return n;
    }
    
    public Network isNetworkPresent(String name){
        for(Network n : networkList){
            if(name.equalsIgnoreCase(n.getName())){
                return n;
            }
        }
        return null;
    }
    
    public void deleteANetwork(Network n){
        networkList.remove(n);
    }
}
