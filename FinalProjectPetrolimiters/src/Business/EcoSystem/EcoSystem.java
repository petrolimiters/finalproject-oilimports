/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.EcoSystem;

import Business.Network.NetworkDirectory;
import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import java.util.ArrayList;

/**
 *
 * @author kaushikp
 */
public class EcoSystem extends Organization{
    
    private static EcoSystem business;
    private NetworkDirectory networkDir;
    private OrganizationDirectory organizationDirectory;
    private ArrayList<UserAccount> masterUserAccountDirectory;
     
    


    public static EcoSystem getInstance(String name) {
        if (business == null) {
            business = new EcoSystem(name);
        }
        return business;
    }

    private EcoSystem(String name) {
        super(name);
        networkDir = new NetworkDirectory();
        organizationDirectory =  new OrganizationDirectory();
        masterUserAccountDirectory = new ArrayList<>();
    }
    
    
    public ArrayList<UserAccount> getMasterUserAccountDirectory() {
        return masterUserAccountDirectory;
    }

    public void setMasterUserAccountDirectory(ArrayList<UserAccount> masterUserAccountDirectory) {
        masterUserAccountDirectory = masterUserAccountDirectory;
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }

    public NetworkDirectory getNetworkDir() {
        return networkDir;
    }

    public void setNetworkDir(NetworkDirectory networkDir) {
        this.networkDir = networkDir;
    }

    
    
       @Override
    public ArrayList<Role> getSupportedRole() {
    return null;
    }
//
//    public boolean checkIfUsernameIsUnique(String username) {
//
//        if (!this.getUserAccountDirectory().checkIfUsernameIsUnique(username)) {
//            return false;
//        }
//
//       
//
//        return true;
//    }
//    
}
