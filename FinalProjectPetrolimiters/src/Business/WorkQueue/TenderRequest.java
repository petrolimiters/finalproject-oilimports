/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.ArrayList;

/**
 *
 * @author kaushikp
 */
public class TenderRequest extends WorkRequest {
    private String name;
    private int oilQuantity;
    private double quotePrice;
    private ArrayList<BiddingRequest> bidsForTender;
    private TenderType tenderType;

    public TenderRequest() {
        super();
        bidsForTender = new ArrayList<>();
    }

    public enum status{
        Open("Open"),
        Closed("Closed");
        
        public String value;
        
        private status(String value){
            this.value= value;
        }

        public String getValue() {
            return value;
        }
        
        @Override
        public String toString(){
            return value;
        }
    }
    
    public enum TenderType{
        Global("Global"),
        Local("Local");
        
        public String value;
        
        private TenderType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
        
        @Override
        public String toString(){
            return value;
        }
    }

    public TenderType getTenderType() {
        return tenderType;
    }

    public void setTenderType(TenderType tenderType) {
        this.tenderType = tenderType;
    }    
    
    public ArrayList<BiddingRequest> getBidsForTender() {
        return bidsForTender;
    }

    public void setBidsForTender(ArrayList<BiddingRequest> bidsForTender) {
        this.bidsForTender = bidsForTender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOilQuantity() {
        return oilQuantity;
    }

    public void setOilQuantity(int oilQuantity) {
        this.oilQuantity = oilQuantity;
    }

    public double getQuotePrice() {
        return quotePrice;
    }

    public void setQuotePrice(double quotePrice) {
        this.quotePrice = quotePrice;
    }
    
    public BiddingRequest searchBidByFrombid(String name){
        System.out.println(bidsForTender.size());
        for(BiddingRequest br : bidsForTender){
            if(br.getBidFrom().toString().equalsIgnoreCase(name)){
                return br;
            }
        }
        return null;
    }
    
    @Override
    public String toString(){
        return name;
    }
    
}
