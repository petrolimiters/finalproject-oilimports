/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author kaushikp
 */
public class BiddingRequest extends WorkRequest{
    
    private Object bidFrom;
    private double bidPrice;
    private String tenderName;
    private static int bidReqCnt=1;
    
    public BiddingRequest(){
        super();
        setRequestId("B00" + bidReqCnt);
        bidReqCnt++;
    }
    public enum status{
        Placed("Placed"),
        Accepted("Accepted"),
        Rejected("Rejected");
        
        public String value;
        
        private status(String value){
            this.value= value;
        }

        public String getValue() {
            return value;
        }
        
        @Override
        public String toString(){
            return value;
        }
    }

    public String getTenderName() {
        return tenderName;
    }

    public void setTenderName(String tenderName) {
        this.tenderName = tenderName;
    }
       
    
    public Object getBidFrom() {
        return bidFrom;
    }

    public void setBidFrom(Object bidFrom) {
        this.bidFrom = bidFrom;
    }

    public double getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(double bidPrice) {
        this.bidPrice = bidPrice;
    }
    
    @Override
    public String toString(){
        return getRequestId().toString();
    }
}
