/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author CHANDNI
 */
public class TransportationRequest extends WorkRequest {
    
   private BiddingRequest biddingRequest;
   private RequestType requestType;

    public BiddingRequest getBiddingRequest() {
        return biddingRequest;
    }

    public void setBiddingRequest(BiddingRequest biddingRequest) {
        this.biddingRequest = biddingRequest;
    }
   private static int transReqCnt =1;
   public enum status{
        Requested("Requested"),
        Dispatch("Dispatch"),
        Delivered("Delivered");
        
        public String value;
        
        private status(String value){
            this.value= value;
        }

        public String getValue() {
            return value;
        }
        
        @Override
        public String toString(){
            return value;
        }
    }
   
   
    public enum RequestType{
        Global("Global"),
        Local("Local");
        
        public String value;
        
        private RequestType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
        
        @Override
        public String toString(){
            return value;
        }
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

   
   public TransportationRequest(){
       super();
       setRequestId("Trns00" + transReqCnt);
       transReqCnt++;
   }
  
    @Override
    public String toString(){
        return getRequestId();
    }
}
