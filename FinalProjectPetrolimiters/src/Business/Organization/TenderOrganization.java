/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.TenderRPRole;
import java.util.ArrayList;

/**
 *
 * @author kaushikp
 */
public class TenderOrganization extends Organization {

    public TenderOrganization() {
        super(OrgType.Tender.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
         ArrayList<Role> roles = new ArrayList<>();
        roles.add(new TenderRPRole());
        return roles;
    }
    
}
