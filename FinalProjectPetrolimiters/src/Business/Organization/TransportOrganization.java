/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.TransportManagerRole;
import java.util.ArrayList;

/**
 *
 * @author kaushikp
 */
public class TransportOrganization extends Organization{

    public TransportOrganization() {
                super(OrgType.Transport.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
         ArrayList<Role> roles = new ArrayList<>();
        roles.add(new TransportManagerRole());
        return roles;
    }
    
}
