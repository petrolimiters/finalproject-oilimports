/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import java.util.ArrayList;

/**
 *
 * @author kaushikp
 */
public class OrganizationDirectory {
 
    private ArrayList<Organization> organizationDirectory;
    
    public OrganizationDirectory(){
        organizationDirectory = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationDirectory() {
        return organizationDirectory;
    }

    public void setOrganizationDirectory(ArrayList<Organization> organizationDirectory) {
        this.organizationDirectory = organizationDirectory;
    }
    
    public Organization addNewOrganization(Organization.OrgType orgType){
        Organization org =null;
        if(orgType == Organization.OrgType.Analysis){
            org = new AnalysisOrganization();
            organizationDirectory.add(org);
        }else if( orgType == Organization.OrgType.Admin){
            org = new AdminOrganization();
            organizationDirectory.add(org);
        }else if( orgType == Organization.OrgType.Bidding ){
            org = new BiddingOrganization();
            organizationDirectory.add(org);
        }else if( orgType == Organization.OrgType.Tender ){
            org = new TenderOrganization();
            organizationDirectory.add(org);
        }else if( orgType == Organization.OrgType.Transport ){
            org = new TransportOrganization();
            organizationDirectory.add(org);
        }
        return org;
    }
    
    public Organization searchOrgByName(String name){
        
        for(Organization o : organizationDirectory){
            if(name.equalsIgnoreCase(o.getName())){
                return o;
            }
        }
        return null;
    }
}
