/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Network.*;

/**
 *
 * @author prathmesh
 */
public class EnterpriselevelAnalysis {
    
     private int netimports;
    private int netexports;
    private double oilglobalprice;
    private double firstpurchaseprice;
    private double importedcrudeoilprice;
    private double enterdomesprice;
    private double enterimportprice;
    private String Year; 


    EnterpriselevelAnalysis() {

    }

    public String getYear() {
        return Year;
    }

    public double getEnterdomesprice() {
        return enterdomesprice;
    }

    public void setEnterdomesprice(double enterdomesprice) {
        this.enterdomesprice = enterdomesprice;
    }

    public double getEnterimportprice() {
        return enterimportprice;
    }

    public void setEnterimportprice(double enterimportprice) {
        this.enterimportprice = enterimportprice;
    }

    
    public void setYear(String Year) {
        this.Year = Year;
    }


    public int getNetimports() {
        return netimports;
    }

    public void setNetimports(int netimports) {
        this.netimports = netimports;
    }

    public int getNetexports() {
        return netexports;
    }

    public void setNetexports(int netexports) {
        this.netexports = netexports;
    }

    public double getOilglobalprice() {
        return oilglobalprice;
    }

    public void setOilglobalprice(double oilglobalprice) {
        this.oilglobalprice = oilglobalprice;
    }

    public double getFirstpurchaseprice() {
        return firstpurchaseprice;
    }

    public void setFirstpurchaseprice(double firstpurchaseprice) {
        this.firstpurchaseprice = firstpurchaseprice;
    }

    public double getImportedcrudeoilprice() {
        return importedcrudeoilprice;
    }

    public void setImportedcrudeoilprice(double importedcrudeoilprice) {
        this.importedcrudeoilprice = importedcrudeoilprice;
    }
    
    
    
}
