/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Network.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import loadCSV.CSV;

/**
 *
 * @author prathmesh
 */
public class EnterpriseanalysisDirectory {
        private ArrayList<EnterpriselevelAnalysis> oilinventory;
        private ArrayList<EnterpriselevelAnalysis> oilinventoryincoming;

        
        public EnterpriseanalysisDirectory(String country) 
                
        {
                    oilinventory = new ArrayList<>();
                    oilinventoryincoming = new ArrayList<>();
                    
                    if(country.equalsIgnoreCase("India"))
                    {
                        try {
                            CSV.loaddataenterindia(this);
                        } catch (IOException ex) {
                            Logger.getLogger(EnterpriseanalysisDirectory.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    
                    else
                    {
                        try { 
                            CSV.loaddataenter(this);
                        } catch (IOException ex) {
                            Logger.getLogger(EnterpriseanalysisDirectory.class.getName()).log(Level.SEVERE, null, ex);
                        }
                                     
                    }
                   
            


        }
        
        public EnterpriselevelAnalysis AddEnterpriseanalysis(double im,double ex,double fpp,double ogp,double icop, String year, double shellimp, double shellexp){
        EnterpriselevelAnalysis n = new EnterpriselevelAnalysis();
        n.setNetimports((int) im);
        n.setFirstpurchaseprice(fpp);
        n.setNetexports((int) ex);
        n.setOilglobalprice(ogp);
        n.setImportedcrudeoilprice(icop);
        n.setYear(year);
        n.setEnterimportprice(shellimp);
        n.setEnterdomesprice(shellexp);
        oilinventory.add(n);
        return n;
    }
   public EnterpriselevelAnalysis addincomingoilinventoryvalues(double im,double ex,double icop, String year){
      
            EnterpriselevelAnalysis matchYear = matchYear(year);
            
            double enterfirsprice = matchYear.getFirstpurchaseprice();
            double domes= enterfirsprice+2.5;
       double imp = icop-1.8;
         EnterpriselevelAnalysis n = new EnterpriselevelAnalysis();
        n.setNetimports((int) im);
        n.setNetexports((int) ex);
        n.setImportedcrudeoilprice(icop);
        n.setYear(year);
        n.setEnterimportprice(imp);
        n.setEnterdomesprice(domes);
        n.setFirstpurchaseprice(matchYear.getFirstpurchaseprice());
        n.setOilglobalprice(matchYear.getOilglobalprice());
        oilinventoryincoming.add(n);
        return n;
    }
        
        
        public EnterpriselevelAnalysis matchYear(String year)
        {
            for(EnterpriselevelAnalysis mn : oilinventory)
            {
                if (mn.getYear().equals(year))
                {
                    return mn;
                }
            }
            return null;
        }

    public ArrayList<EnterpriselevelAnalysis> getOilinventoryincoming() {
        return oilinventoryincoming;
    }

    public void setOilinventoryincoming(ArrayList<EnterpriselevelAnalysis> oilinventoryincoming) {
        this.oilinventoryincoming = oilinventoryincoming;
    }
        
        

    public ArrayList<EnterpriselevelAnalysis> getOilinventory() {
        return oilinventory;
    }

    public void setOilinventory(ArrayList<EnterpriselevelAnalysis> oilinventory) {
        this.oilinventory = oilinventory;
    }
        
        

    
}
