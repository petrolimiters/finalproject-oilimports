/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author kaushikp
 */
public class Enterprise extends Organization{
    
    private OrganizationDirectory orgDir;
    private String entId;
        private EnterpriseanalysisDirectory enteranadir;

    private static int entCount =0;
  
    public Enterprise(String name, String country){
        super(name);
        orgDir = new OrganizationDirectory();
        entId = "ENT00" + ++entCount;
        enteranadir = new EnterpriseanalysisDirectory(country);
    }

    public EnterpriseanalysisDirectory getEnteranadir() {
        return enteranadir;
    }

    public void setEnteranadir(EnterpriseanalysisDirectory enteranadir) {
        this.enteranadir = enteranadir;
    }
    

    public OrganizationDirectory getOrgDir() {
        return orgDir;
    }

    public void setOrgDir(OrganizationDirectory orgDir) {
        this.orgDir = orgDir;
    }
    
    @Override
    public  String toString(){
        return getName();
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
