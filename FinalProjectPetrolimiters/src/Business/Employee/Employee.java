/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

/**
 *
 * @author kaushikp
 */
public class Employee {
 
    private String name;
    private String empId;
    private int count=0;

    public Employee(){
        empId = "Emp00" + ++count;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }
    
    @Override
    public String toString(){
        return name;
    }
}
