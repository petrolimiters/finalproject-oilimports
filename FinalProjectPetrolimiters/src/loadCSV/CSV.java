/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loadCSV;

import Business.Enterprise.EnterpriseanalysisDirectory;
import Business.Network.networkanalysisDirectory;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author prathmesh
 */
public class CSV {   
    
    public static void loaddata(networkanalysisDirectory nadir) throws FileNotFoundException, IOException {

        BufferedReader br = null;
        String header = "";
        String row = "";
        String csvSplitBy = ",";

        String path = "csv/prevcrudeoildata.csv";
        br = new BufferedReader(new FileReader(path));
        header = br.readLine();
        while ((row = br.readLine()) != null) {
            String[] CSVanalysisnetworkrow = row.split(csvSplitBy);
                nadir.AddNetworkanalysis(Double.parseDouble(CSVanalysisnetworkrow[1]), Double.parseDouble(CSVanalysisnetworkrow[2]),Double.parseDouble(CSVanalysisnetworkrow[4]),Double.parseDouble(CSVanalysisnetworkrow[3]),Double.parseDouble(CSVanalysisnetworkrow[5]), CSVanalysisnetworkrow[6] );

        }
    }
    
    
     public static void loaddataenter(EnterpriseanalysisDirectory enternadir) throws FileNotFoundException, IOException {

        BufferedReader br = null;
        String header = "";
        String row = "";
        String csvSplitBy = ",";

        String path = "csv/prevcrudeoildata_enterprise.csv";
        br = new BufferedReader(new FileReader(path));
        header = br.readLine();
        while ((row = br.readLine()) != null) {
            String[] CSVanalysisnetworkrow = row.split(csvSplitBy);
                enternadir.AddEnterpriseanalysis(Double.parseDouble(CSVanalysisnetworkrow[1]), Double.parseDouble(CSVanalysisnetworkrow[2]),Double.parseDouble(CSVanalysisnetworkrow[4]),Double.parseDouble(CSVanalysisnetworkrow[3]),Double.parseDouble(CSVanalysisnetworkrow[5]), CSVanalysisnetworkrow[6], Double.parseDouble(CSVanalysisnetworkrow[7]), Double.parseDouble(CSVanalysisnetworkrow[8]));
        }
    }
     
     
     
     
     
       
    public static void loaddataindia(networkanalysisDirectory nadir) throws FileNotFoundException, IOException {

        BufferedReader br = null;
        String header = "";
        String row = "";
        String csvSplitBy = ",";

        String path = "csv/prevcrudeoildataindia.csv";
        br = new BufferedReader(new FileReader(path));
        header = br.readLine();
        while ((row = br.readLine()) != null) {
            String[] CSVanalysisnetworkrow = row.split(csvSplitBy);
                nadir.AddNetworkanalysis(Double.parseDouble(CSVanalysisnetworkrow[1]), Double.parseDouble(CSVanalysisnetworkrow[2]),Double.parseDouble(CSVanalysisnetworkrow[4]),Double.parseDouble(CSVanalysisnetworkrow[3]),Double.parseDouble(CSVanalysisnetworkrow[5]), CSVanalysisnetworkrow[6] );

        }
    }
    
    
     public static void loaddataenterindia(EnterpriseanalysisDirectory enternadir) throws FileNotFoundException, IOException {

        BufferedReader br = null;
        String header = "";
        String row = "";
        String csvSplitBy = ",";

        String path = "csv/prevcrudeoildataenterpriseindia.csv";
        br = new BufferedReader(new FileReader(path));
        header = br.readLine();
        while ((row = br.readLine()) != null) {
            String[] CSVanalysisnetworkrow = row.split(csvSplitBy);
                enternadir.AddEnterpriseanalysis(Double.parseDouble(CSVanalysisnetworkrow[1]), Double.parseDouble(CSVanalysisnetworkrow[2]),Double.parseDouble(CSVanalysisnetworkrow[4]),Double.parseDouble(CSVanalysisnetworkrow[3]),Double.parseDouble(CSVanalysisnetworkrow[5]), CSVanalysisnetworkrow[6], Double.parseDouble(CSVanalysisnetworkrow[7]), Double.parseDouble(CSVanalysisnetworkrow[8]));
        }
    }
     
     
    
}
